# Strapi Webhook To Telegram Bot

A Telegram bot, which notifies the configured chat on new events. At the moment it is very specific with few options for configurations.

## Prerequisites
This guide assumes you have a current version of `node` and `npm` installed.

## Setup
* Do the technical stuff:
```
git clone https://gitlab.com/cs-ch/website/strapi-webhook-to-telegram-bot
cd strapi-webhook-to-telegram-bot
npm i
npm run build
cp example.env .env
```
* If you havent already, create your bot by talking to [BotFather](https://t.me/BotFather). You will recieve a token. Enter this token into your `.env`-file after `BOT_TOKEN=`.
* Start the application with `npm start`.
* Add the bot to the desired chat and send `/chatId` to the chat. The bot will respond with something like `The id of this chat is: 123456`. Enter the chat id into your `.env`-file after `CHAT_ID=`.
* Restart the application - ideally you configure it as a process with either `systemd`, `pm2` or something simmilar.
* In your Strapi app, go under Settings - Webhooks and add a new webhook. Replace `PORT` and `PATHNAME` in the Url field with the respective configurations in your `.env` file. Save the webhook.
*Note: If your application is running on a different server, you'll need to replace "localhost" with the respective url*.
![Configuration in Strapi](./docs/configure_strapi.png)
* That's it. The bot should now notify you on added events.

## Configuration Options
There are the following configuration options, as visible in the [example.env](../example.env) file:
* `BOT_TOKEN`: The token of your telegram bot. Keep this token private, because otherwise people could take control over your bot. You recieve the token after creating your bot - which can be done by talking to [BotFather](https://t.me/BotFather)
* `PORT`: The port the `http` server listens on.
* `PATHNAME`: The pathname/route the `http` server listens on.
* `CHAT_ID`: The id of the chat the bot writes to on a new event. For this, add the bot to the desired chat and send `/chatId` to the chat. The bot will respond with something like `The id of this chat is: 123456`.
* `STRAPI_URL`: The url of the cms, which is added for convenience to the notification.

## Contributions
For consistent code style, please run `npm run format` before comitting.

Other than that, the contribution guide of our other repos apply, see [here](https://gitlab.com/cs-ch/website/climatestrike-website/-/blob/development/CONTRIBUTING.md).
