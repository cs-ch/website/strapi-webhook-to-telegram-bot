require("dotenv").config();
import http from "http";
import {Telegraf} from "telegraf";

import {RequestHandler} from "./RequestHandler";

const bot = new Telegraf(process.env.BOT_TOKEN as string);

bot.command("/chatId", (ctx) => {
  ctx.reply(`The id of this chat is: ${ctx.chat.id}`);
});

bot.launch();
bot.start((ctx) => console.log("Bot started"));

process.once("SIGINT", () => bot.stop("SIGINT"));
process.once("SIGTERM", () => bot.stop("SIGTERM"));

const handleBody = (
  req: http.IncomingMessage,
  callback: (body: any) => void,
) => {
  let body = "";
  req.on("data", (chunk) => {
    body += chunk;
  });
  req.on("end", () => {
    console.debug("Parsed body: " + body);
    callback(JSON.parse(body));
  });
};

const requestHandler = new RequestHandler(bot, process.env.CHAT_ID as string);
const server = http.createServer((req, res) => {
  try {
    const {method, url} = req;
    console.info(`[${new Date().toISOString()}] "${method} - ${url}"`);
    const allowedPathname = process.env.PATHNAME || "";

    if (method !== "POST" || url !== allowedPathname) {
      res.writeHead(405, {"Content-Type": "application/json"});
      res.end(
        JSON.stringify({
          success: false,
          message: `Only "POST" allowed to "${allowedPathname}"`,
        }),
      );
    }

    handleBody(req, (body) => requestHandler.handle(body));

    res.writeHead(200, {"Content-Type": "application/json"});
    res.end(JSON.stringify({success: true}));
  } catch (err) {
    console.error("An unexpected error occurred: " + err);
    res.writeHead(500, {"Content-Type": "application/json"});
    res.end(JSON.stringify({success: false}));
  }
});
console.log("Starting server on port: " + process.env.PORT);
server.listen(process.env.PORT);
