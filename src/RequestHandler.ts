import {Telegraf} from "telegraf";

interface IStrapiEvent {
  event: "entry.create" | "entry.update"; // More we don't care about
  created_at: string;
  model: "event" | "event-group"; // More we don't care about
  entry: {
    identifier: string;
    description: {language: string; text: string}[];
    start: string;
    end: string;
  };
}

export class RequestHandler {
  constructor(
    private bot: Telegraf,
    private chatId: string,
  ) {}

  public handle(body: IStrapiEvent) {
    const {event, model, entry} = body;
    console.debug(`Handling event "${event}" for model "${model}"`);
    if (event === "entry.create" && model === "event") {
      const {identifier, description = [], start, end} = entry;
      const message = `A new event has been added:

<b>identifier:</b> ${identifier}
<b>start:</b> ${start}
<b>end:</b> ${end || "-"}
<b>descriptions:</b> ${description.map((item) => item.text).join(" // ")}

Please visit <a href="${process.env.STRAPI_URL}">${
        process.env.STRAPI_URL
      }</a> to publish it`;
      this.bot.telegram.sendMessage(this.chatId, message, {
        parse_mode: "HTML",
      });
    }
  }
}
